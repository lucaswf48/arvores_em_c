#include <stdio.h>
#include <stdlib.h>
#include "arvores.h"


Arv* arvoreInicializa(void){

	return NULL;
}

Arv* arvoreCria(char c, Arv* sae, Arv* sad){

	Arv* p=(Arv*)malloc(sizeof(Arv));
	p->info = c;
	p->esq = sae;
	p->dir = sad;
	return p;
}

int arvoreVazia(Arv* a){

	return a==NULL;
}

void arvoreImprimePre (Arv* a){

	if (!arvoreVazia(a)){
	printf("%c ", a->info); /* mostra raiz */
	arvoreImprimePre(a->esq); /* mostra sae */
	arvoreImprimePre(a->dir); /* mostra sad */
	}
}

void arvoreImprimeSim(Arv* a){

	if(!arvoreVazia(a)){


	arvoreImprimeSim(a->esq); /* mostra sae */
	printf("%c ", a->info); /* mostra raiz */
	arvoreImprimeSim(a->dir); /* mostra sad */
	}
}

void arvoreImprimePos(Arv* a){

	if(!arvoreVazia(a)){


	arvoreImprimePos(a->esq); /* mostra sae */

	arvoreImprimePos(a->dir); /* mostra sad */
	printf("%c ", a->info); /* mostra raiz */
	}
}

Arv* arvoreLibera (Arv* a){

	if (!arvoreVazia(a)){

		arvoreLibera(a->esq); /* libera sae */
		arvoreLibera(a->dir); /* libera sad */
		free(a); /* libera raiz */
	}
	return NULL;
}

int arvoreBusca(Arv* a, char c){
	if (arvoreVazia(a))
		return 0; /* árvore vazia: não encontrou */
	else{
		//printf("%d",a->info==c || busca(a->esq,c) || busca(a->dir,c));
		return a->info==c || arvoreBusca(a->esq,c) || arvoreBusca(a->dir,c);
	}
}

int arvoreMax2(int a, int b){

	return (a > b) ? a : b;
}

int arvoreAltura(Arv *a){

	if(arvoreVazia(a))
		return -1;
	else
		return 1 + arvoreMax2(arvoreAltura(a->esq), arvoreAltura(a->dir));
}


Arv* arvoreInsere(Arv *a,char carac){

	if(a == NULL){

		a = (Arv*) malloc(sizeof(Arv));
		a->info = carac;
		a->esq = a->dir = NULL;
	}
	else if(carac < a->info)
		a->esq = arvoreInsere(a->esq,carac);
	else
		a->dir = arvoreInsere(a->dir,carac);
	return a;
}


void arvoreContaFolhasFilhos(Arv *p,int *folhas,int *filho1,int *filho2){

    if(!arvoreVazia(p)){
        if(p->esq == NULL && p->dir==NULL){

            ++(*folhas);

        }
        else if((p->esq == NULL && p->dir!=NULL) || (p->esq != NULL && p->dir==NULL)){
            ++(*filho1);
        }
        else if(p->esq != NULL && p->dir != NULL){
            ++(*filho2);
        }
        arvoreContaFolhasFilhos(p->esq,folhas,filho1,filho2);
        arvoreContaFolhasFilhos(p->dir,folhas,filho1,filho2);
    }

}


////////////////////////////////////////////////////////////////////////////////////////////////

ArvVar *arvv_cria(char c){

	ArvVar *a = (ArvVar*) malloc(sizeof(ArvVar));
	a->info = c;
	a->prim = NULL;
	a->prox = NULL;
	return a;

}

void arvv_insere(ArvVar *a,ArvVar *sa){

	sa->prox = a->prim;
	a->prim = sa;
}

void arvv_imprime(ArvVar *a){

	ArvVar *p;
	printf("%c \n", a->info);
	for(p = a->prim; p!= NULL; p = p->prox)
		arvv_imprime(p);
}


int arvv_pertence(ArvVar *a, char c){
	ArvVar *p;
	if(a->info == c){
		return 1;
	}
	else{
		for(p = a->prim; p!= NULL;p= p->prox){
			if(arvv_pertence(p,c))
				return 1;
		}
		return 0;
	}
}

void arvv_libera(ArvVar *a){

	ArvVar *p = a->prim;

	while(p != NULL){

		ArvVar *t = p->prox;
		arvv_libera(p);
		p = t;
	}
	free(a);
}

int arvv_altura(ArvVar *a){

	int hmax= -1;
	ArvVar *p;

	for(p = a->prim;p!=NULL;p = p->prox){

		int h = arvv_altura(p);
		if(h > hmax)
			hmax = h;
	}

	return hmax + 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////

void arvoreAvlRotacionaDireita(pno p){

	pno q, temp;
	q = p->esq;
	temp = q->dir;
	q->dir = p;
	p->esq = temp;
	p = q;
}

void arvoreAvlRotacionaEsquerda(pno p){

	pno q,temp;
	q = p->dir;
	temp = q->esq;
	q->esq = p;
	p->dir = temp;
	p = q;
}

void arvoreAvlRotacionaEsquerdaDireita(pno p){

	pno u,v;
	u = p->esq;
	v = u->dir;
	u->dir = v->esq;
	v->esq = u;
	p->esq = v;
	v->dir = p;
	if(v->bal == -1){
		u->bal = 0;
		p->bal = 1;
	}
	else{

		p->bal = 0;
		u->bal = -1;
	}
	p = v;
}

void arvoreAvlRotacionaDireitaEsquerda(pno p){

	pno z,v;
	z = p->dir;
	v = z ->esq;
	z->esq = v->dir;
	v->dir = z;
	p->dir = v->esq;
	v->esq = p;
	if(v->bal == 1){
		p->bal = -1;
		z->bal = 0;
	}
	else{

		p->bal = 0;
		z->bal = 1;
	}

	p = v;
}

void arvoreAvlInsere(int x, pno p,bool kank){

	if(p == NULL){

		arvoreAvlAloca(p,x);
		kank = true;
		return;
	}
	if(x < p->info){
		arvoreAvlInsere(x,p->esq,kank);
		if(kank)
			switch(p->bal){
				case 1:
						p->bal = 0;
						kank = false;
						break;
				case 0:
						p->bal = -1;
						break;
				case -1:
						arvoreAvlCaso1(p);
						kank = false;
						break;
			}
		return;
	}

	if(x > p->info){

		arvoreAvlInsere(x,p->dir,kank);
		if(kank)
			switch(p->bal){
				case -1:
						p->bal = 0;
						kank = false;
						break;
				case 1:
						arvoreAvlCaso2(p);
						kank = false;
						break;
			}
		return;
	}
}

void arvoreAvlAloca(pno p, int x){

	p = malloc(sizeof(no));
	p->esq = NULL;
	p->dir = NULL;
	p->info = x;
	p->bal = 0;
}

void arvoreAvlCaso1(pno p){

	pno u;
	u = p->esq;
	if(u->bal == -1)
		arvoreAvlRotacionaDireita(p);
	else
		arvoreAvlRotacionaEsquerdaDireita(p);

	p->bal = 0;
}

void arvoreAvlCaso2(pno p){

	pno u;
	u = p->dir;
	if(u->bal == 1)
		arvoreAvlRotacionaEsquerda(p);
	else
		arvoreAvlRotacionaDireitaEsquerda(p);
	p->bal = 0;
}