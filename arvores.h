#include <stdbool.h>

struct arv {
 char info;
 struct arv* esq;
 struct arv* dir;
};

typedef struct arv Arv;

struct arvvar{
	char info;
	struct arvvar *prim;
	struct arvvar *prox;
};

typedef struct arvvar ArvVar;

typedef struct no *pno;

typedef struct no{

	int bal;
	int info;
	pno dir,esq;
}no;

typedef pno tree;

tree raiz;

Arv* arvoreInicializa(void);
Arv* arvoreCria(char c, Arv* sae, Arv* sad);
int arvoreVazia(Arv* a);
void arvoreImprimePre (Arv* a);
void arvoreImprimeSim(Arv* a);
void arvoreImprimePos(Arv* a);
Arv* arvoreInsere(Arv *a,char carac);
Arv* arvoreLibera (Arv* a);
int arvoreBusca (Arv* a, char c);
int arvoreMax2 (int a ,int b);
void arvoreContaFolhasFilhos(Arv *p,int *folhas,int *filho1,int *filho2);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

int arv_altura(Arv *a);
ArvVar *arvv_cria(char c);
void arvv_insere(ArvVar *a,ArvVar *sa);
void arvv_imprime(ArvVar *a);
int arvv_pertence(ArvVar *a, char c);
void arvv_libera(ArvVar *a);
int arvv_altura(ArvVar *a);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void arvoreAvlRotacionaDireita(pno p);
void arvoreAvlRotacionaEsquerda(pno p);
void arvoreAvlRotacionaEsquerdaDireita(pno p);
void arvoreAvlRotacionaDireitaEsquerda(pno p);
void arvoreAvlInsere(int x, pno p,bool kank);
void arvoreAvlAloca(pno p, int x);
void arvoreAvlCaso1(pno p);
void arvoreAvlCaso2(pno p);
